import com.devcamp.S10.jbr210.Author;
import com.devcamp.S10.jbr210.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Ninh", "ninh@123", 'M');
        Author author2 = new Author("Hao", "hao.@123", 'F');
        System.out.println(author1.toString());
        System.out.println(author2.toString());

        Book book1 = new Book("Java", author1, 150.5);
        Book book2 = new Book("React Js", author2, 100, 3);
        System.out.println(book1.toString());
        System.out.println(book2.toString());
    }
}
